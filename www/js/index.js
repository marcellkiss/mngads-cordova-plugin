/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        this.initMngAds();
        
    },
    mngadsAppId: function() {
    	return ( /(android)/i.test(navigator.userAgent) ) ?'YOUR_ANDROID_APPID':'YOUR_IOS_APPID';
    },
    initMngAds: function() {
        // Set your AppId.
        this.mngAds = new MngAdsSDK();
        if (! this.mngAds ) { alert( 'mngAds plugin not ready' ); return; }
        // Connection to the API.
      this.mngAds.setDebugMode(true);
      this.mngAds.initBeacons();
        this.mngAds.initWithAppId(this.mngadsAppId(),this.onInitialize);
        this.mngAds.isInitialized(this.isInitialized);
        document.getElementById("status-ads").innerHTML="mngAds is ready ! ";
        
    },
    removeNative: function() {
  
    this.mngAds.removeNative(this.adSuccess,this.adError);
      
    },

    createInterstitial: function(autoDispaly) {
  var age = "25";
  var language = "fr";
  var keyword="brand=myBrand;category=sport;";
  var gender="M";
  var location={"lat":48.876,"lon":10.453};
  var preferences = {
     "age": age,
     "language": language, 
       "keyword":keyword,
       "gender":gender,
       "location":location      
  }
      
    // Set your placementId.
    var placementId = '/'+this.mngadsAppId()+'/interstitial';
    document.getElementById("status-ads").innerHTML="waiting Interstitial "+placementId ;
    this.mngAds.createInterstitial(placementId,preferences,this.adSuccess,this.adError,autoDispaly);
      
    },


    showInterstitial: function() {
    document.getElementById("status-ads").innerHTML="waiting Interstitial " ;
    this.mngAds.showInterstitial(function(message){
      document.getElementById("status-ads").innerHTML="Interstitial displayed" ;
    },
    function(message) {
      document.getElementById("status-ads").innerHTML="Fail to display " ;
    });
      
    },


    createBanner: function(id,size,position) {
  var age = "25";
  var language = "fr";
  var keyword="brand=myBrand;category=sport;";
  var gender="M";
  var location={"lat":48.876,"lon":10.453};
  var preferences = {
       "age": age,
       "language": language, 
     "keyword":keyword,
       "gender":gender,
     "location":location
        }
      // Set your placementId.
      var placementId = '/'+this.mngadsAppId()+'/homebanner';
      document.getElementById("status-ads").innerHTML="waiting banner "+placementId ;
      this.mngAds.createBanner(placementId,size,position,true,preferences,this.adSuccess,this.adError)
    },
    showBanner: function() {
      this.mngAds.showBanner(this.adSuccess,this.adError);
    }, 
    isInitialized: function(ok) {
        if (ok == true) {
          console.log("MNGAds is initialized");     
      }else{
           console.log("MNGAds is not initialized");   
      }
    }, 
    createNative: function() {
  var age = "25";
  var language = "fr";
  var keyword="brand=myBrand;category=sport;";
  var gender="M";
  var location={"lat":48.876,"lon":10.453};
  var preferences = {
       "age": age,
       "language": language, 
     "keyword":keyword,
       "gender":gender,
     "location":location
        }
      // Set your placementId.
      var placementId = '/'+this.mngadsAppId()+'/nativead';
      document.getElementById("status-ads").innerHTML="waiting Native "+placementId ;
      this.mngAds.createNative(placementId,preferences,this.nativeAdSuccess,this.adError)
    },  
      nativeAdSuccess: function(nativeObject) {
      
       document.getElementById("status-ads").innerHTML="success - didLoad";
      var nativeObjectArray = JSON.parse(nativeObject);
  
     // NATIVE_TITLE : nativeObjectArray.title
     // NATIVE_BODY  : nativeObjectArray.body
     // NATIVE_SOCIAL_CONTEXT : nativeObjectArray.social_context
     // NATIVE_CALL_TO_ACTION : nativeObjectArray.call_to_action
     // NATIVE_AD_ICON : nativeObjectArray.ad_icon_url
     // NATIVE_AD_COVER_IMAGE : nativeObjectArray.ad_cover_url
     // NATIVE_PRICE_TEXT : nativeObjectArray.price_text
     // NATIVE_PRICE_TYPE : nativeObjectArray.price_type  return free ,payable,unknown
     // NATIVE_BADGE_URL : nativeObjectArray.badge_url
   
    var nativeAdContent =' <table style="width:100%;margin: 0 0;"><tr> <td><img style="width:50px;height:50px;margin: 0 0;" src='+nativeObjectArray.ad_icon_url+'></td> <td><h4 style ="color:red;line-height: 90%;margin: 0 0;">'+nativeObjectArray.title + '</h4></td> <td valign="top"><img src="'+nativeObjectArray.badge_url+'"style="width:50px;height:15px align="right"></td> </tr></table><h6 style ="margin: 0 0;">'+nativeObjectArray.body+'</h6><div id="mediaContainer" style="height: 200px; width: 100%; border: 1px    solid black;"></div><button id="NativeClick" class="button button-block button-rounded button-ads button-large">'+nativeObjectArray.call_to_action+'</button>';
  

  document.getElementById("NativeDiv").innerHTML=nativeAdContent ;
  
  app.updateMediaContainerArea();
  

  app.updateClickArea();

  window.addEventListener( "scroll", function( event ) {
        app.updateClickArea();
        app.updateMediaContainerArea();
       });

    }, 
    updateMediaContainerArea :function(){
      var elem = document.getElementById('mediaContainer'),
      properties = window.getComputedStyle(elem, null),           
  x=elem.offsetLeft-document.body.scrollLeft;
  y=elem.offsetTop-document.body.scrollTop;     
  w = parseInt(properties.width, 10);
  h = parseInt(properties.height, 10);
      this.mngAds.setMediaContainerArea(x, y, w, h);
    },

    updateClickArea :function (){
  // change the click area
  var elem = document.getElementById('NativeClick'),
      properties = window.getComputedStyle(elem, null),           
  x=elem.offsetLeft-document.body.scrollLeft;
  y=elem.offsetTop-document.body.scrollTop;     
  w = parseInt(properties.width, 10);
  h = parseInt(properties.height, 10);
      this.mngAds.setNativeAdClickArea(x, y, w, h);
      
    },
     onInitialize: function() {
  
     console.log("MNGAds is initialized"); 

       app.createInterstitial(true);
    },  
    adError: function(error) {
       document.getElementById("status-ads").innerHTML=error;
    },
    adSuccess: function(eventType) {
      if (eventType == "DID_LOAD") {
          console.log("didLoad");
          document.getElementById("status-ads").innerHTML="success - didLoad";
      }
      else if (eventType == "REMOVE_NATIVE") 
      {
          console.log("Remove native");
          document.getElementById("NativeDiv").innerHTML="";
          document.getElementById("status-ads").innerHTML="native - didDisappear";

      }   
      else{
           console.log("didDisappear");
           //document.getElementById("status-ads").innerHTML="success - didDisappear";
      }
  }

};



function clickAutoDisplay(){
    if (document.getElementById("autoDispaly").checked) {
        document.getElementById("btnShow").setAttribute('style', 'display:none;');
    }else{
        document.getElementById("btnShow").setAttribute('style', 'display:inline;');
    };

}



