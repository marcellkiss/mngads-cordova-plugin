/*!
 *  @header    CDVMngAdsSDK.h
 *  @abstract  Cordova Plugin for the MngAds iOS SDK.
 *  @version   1.0.10
 */

// Cordova
#import <Cordova/CDVPlugin.h>

// MngAds SDK
#import "MNGAdsSDKFactory.h"


@interface CDVMngAdsSDK : CDVPlugin<MNGAdsAdapterInterstitialDelegate,MNGAdsAdapterBannerDelegate>

- (void)mngadssdk_initWithAppId:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_initBeacons:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_isInitialized:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_debugEnable:(CDVInvokedUrlCommand *)command;
//INTERSTITIAL
- (void)mngadssdk_createInterstitial:(CDVInvokedUrlCommand *)command;
//BANNER
- (void)mngadssdk_createBanner:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_showBanner:(CDVInvokedUrlCommand *)command;
//NATIVE
- (void)mngadssdk_createNative:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_setNativeAdClickArea:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_removeNativeAd:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_setMediaContainerArea:(CDVInvokedUrlCommand *)command;
- (void)mngadssdk_showInterstitial:(CDVInvokedUrlCommand *)command;

@end
