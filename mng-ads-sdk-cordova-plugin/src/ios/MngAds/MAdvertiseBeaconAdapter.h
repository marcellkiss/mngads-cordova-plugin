//
//  MNGBeaconAdapter.h
//  MngAds
//
//  Created by MacBook Pro on 10/10/2016.
//  Copyright © 2016 Bensalah Med Amine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAdvertiseBeaconAdapter : NSObject

+(MAdvertiseBeaconAdapter*)singleton;

-(void)initBeaconWithAppId:(NSString*)appId;

-(void)handleNotificationWithUserInfo:(NSDictionary*)userInfo;

-(BOOL)userInfoIsBeaconNotification:(NSDictionary*)userInfo;

@end
